# stio

Single-Threaded, I/O.

[![build status](https://gitlab.com/ahmedcharles/stio/badges/main/pipeline.svg)
](https://gitlab.com/ahmedcharles/stio/commits/main)
[![Crates.io](https://img.shields.io/crates/v/stio.svg)
](https://crates.io/crates/stio)

## Usage

Add this to your `Cargo.toml` (crates.io):

```toml
[dependencies]
stio = "0.0.1"
```

or (git):

```toml
[dependencies]
stio = { git = "https://gitlab.com/ahmedcharles/stio" }
```

Get the latest version from [GitLab](https://gitlab.com/ahmedcharles/stio).

```
   Copyright 2024 Ahmed Charles <me@ahmedcharles.com>

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
