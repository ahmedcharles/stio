// Copyright 2024 Ahmed Charles <me@ahmedcharles.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use std::{
    io::{Read as _, Write as _},
    net::TcpStream,
};

fn run(port: u16, data: &[u8]) {
    let mut stream = TcpStream::connect(("127.0.0.1", port)).unwrap();

    stream.write(data).unwrap();

    println!("sent {} bytes {data:x?} on {port}", data.len());

    let mut buf = [0; 10];
    match stream.read(&mut buf) {
        Ok(received) => println!(
            "received {received} bytes {:x?} on {port}",
            &buf[..received]
        ),
        Err(e) => println!("recv function failed: {e:?} on {port}"),
    }
}

fn main() {
    run(8000, b"foo");
    run(8001, b"bar");
}
