// Copyright 2024 Ahmed Charles <me@ahmedcharles.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use stio::{
    net::TcpListener,
    runtime::{Handle, Runtime},
};

async fn async_main(handle: Handle, port: u16) -> usize {
    let listener = TcpListener::bind(handle, ([127, 0, 0, 1], port).into()).unwrap();
    let mut r = 0;

    for _ in 0..2 {
        let (stream, _) = listener.accept().await.unwrap();
        let mut buf = [0; 10];
        let amt = stream.read(&mut buf).await.unwrap();

        let buf = &mut buf[..amt];
        buf.reverse();
        stream.write(buf).await.unwrap();
        r += buf.len();
    }
    r
}

fn main() {
    let runtime = Runtime::new().unwrap();
    let handle = runtime.handle();
    let h = handle.clone();
    handle.spawn(async {
        let _ = async_main(h, 8000).await;
    });
    let r = runtime.block_on(async_main(handle, 8001));
    println!("received {r} bytes");
}
