// Copyright 2024 Ahmed Charles <me@ahmedcharles.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! TCP/UDP bindings for `stio`.

use crate::runtime::Handle;

use std::{
    future::poll_fn,
    io::{ErrorKind, Read as _, Result as IoResult, Write as _},
    net::{Shutdown, SocketAddr},
    pin::Pin,
    task::{ready, Context, Poll},
};

use mio::{
    net::TcpListener as MioTcpListener, net::TcpStream as MioTcpStream,
    net::UdpSocket as MioUdpSocket, Interest, Token,
};

/// A TCP socket server, listening for connections.
#[derive(Debug)]
pub struct TcpListener {
    /// Underlying Mio [`TcpListener`].
    ///
    /// [`TcpListener`]: mio::net::TcpListener
    listener: MioTcpListener,
    /// Mio event Token used for registration and polling.
    token: Token,
    /// The [`Handle`] to the current [`Runtime`].
    ///
    /// [`Runtime`]: crate::runtime::Runtime
    handle: Handle,
}

impl TcpListener {
    /// Creates a new `TcpListener`, which will be bound to the specified address.
    pub fn bind(handle: Handle, addr: SocketAddr) -> IoResult<Self> {
        let mut listener = MioTcpListener::bind(addr)?;

        let token = handle.rt().register(&mut listener, Interest::READABLE)?;

        Ok(TcpListener {
            listener,
            token,
            handle,
        })
    }

    /// Accepts a new incoming connection from this listener.
    pub async fn accept(&self) -> IoResult<(TcpStream, SocketAddr)> {
        loop {
            match self.listener.accept() {
                Ok((stream, addr)) => {
                    return Ok((TcpStream::from_mio(self.handle.clone(), stream)?, addr))
                }
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    poll_fn(|cx| self.handle.rt().poll(self.token, Interest::READABLE, cx)).await?;
                }
                Err(error) => return Err(error),
            }
        }
    }
}

impl Drop for TcpListener {
    fn drop(&mut self) {
        let _drop = self.handle.rt().deregister(self.token, &mut self.listener);
    }
}

/// A TCP stream between a local and remote socket.
#[derive(Debug)]
pub struct TcpStream {
    /// Underlying Mio [`TcpStream`].
    ///
    /// [`TcpStream`]: mio::net::TcpStream
    stream: MioTcpStream,
    /// Mio event Token used for registration and polling.
    token: Token,
    /// The [`Handle`] to the current [`Runtime`].
    ///
    /// [`Runtime`]: crate::runtime::Runtime
    handle: Handle,
}

impl TcpStream {
    /// Creates a new `TcpStream` from a `mio::net::TcpStream`.
    fn from_mio(handle: Handle, mut stream: MioTcpStream) -> IoResult<Self> {
        let token = handle
            .rt()
            .register(&mut stream, Interest::READABLE | Interest::WRITABLE)?;

        Ok(TcpStream {
            stream,
            token,
            handle,
        })
    }

    /// Opens a TCP connection to a remote host.
    pub async fn connect(handle: Handle, addr: SocketAddr) -> IoResult<Self> {
        let mut stream = MioTcpStream::connect(addr)?;

        let token = handle
            .rt()
            .register(&mut stream, Interest::READABLE | Interest::WRITABLE)?;

        loop {
            poll_fn(|cx| handle.rt().poll(token, Interest::WRITABLE, cx)).await?;

            match stream.peer_addr() {
                Ok(_) => {
                    return Ok(TcpStream {
                        stream,
                        token,
                        handle,
                    })
                }
                Err(e) if e.kind() == ErrorKind::NotConnected => {}
                Err(e) if e.kind() == ErrorKind::WouldBlock => {}
                Err(e) => return Err(e),
            }
        }
    }

    /// Attempts to read from the `TcpStream` into `buf`.
    pub fn poll_read(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut [u8],
    ) -> Poll<IoResult<usize>> {
        loop {
            match (&self.stream).read(buf) {
                Ok(value) => return Poll::Ready(Ok(value)),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    match ready!(self.handle.rt().poll(self.token, Interest::READABLE, cx)) {
                        Ok(()) => {}
                        Err(e) => return Poll::Ready(Err(e)),
                    }
                }
                Err(error) => return Poll::Ready(Err(error)),
            }
        }
    }

    /// Attempts to read from the `TcpStream` into `buf`.
    pub async fn read(&self, buf: &mut [u8]) -> IoResult<usize> {
        loop {
            match (&self.stream).read(buf) {
                Ok(value) => return Ok(value),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    poll_fn(|cx| self.handle.rt().poll(self.token, Interest::READABLE, cx)).await?;
                }
                Err(error) => return Err(error),
            }
        }
    }

    /// Attempts to write from `buf` into the `TcpStream`.
    pub fn poll_write(
        self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<IoResult<usize>> {
        loop {
            match (&self.stream).write(buf) {
                Ok(value) => return Poll::Ready(Ok(value)),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    match ready!(self.handle.rt().poll(self.token, Interest::WRITABLE, cx)) {
                        Ok(()) => {}
                        Err(e) => return Poll::Ready(Err(e)),
                    }
                }
                Err(error) => return Poll::Ready(Err(error)),
            }
        }
    }

    /// Attempts to write from `buf` into the `TcpStream`.
    pub async fn write(&self, buf: &[u8]) -> IoResult<usize> {
        loop {
            match (&self.stream).write(buf) {
                Ok(value) => return Ok(value),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    poll_fn(|cx| self.handle.rt().poll(self.token, Interest::WRITABLE, cx)).await?;
                }
                Err(error) => return Err(error),
            }
        }
    }

    /// Attempts to flush this `TcpStream`.
    pub fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<IoResult<()>> {
        loop {
            match (&self.stream).flush() {
                Ok(()) => return Poll::Ready(Ok(())),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    match ready!(self.handle.rt().poll(self.token, Interest::WRITABLE, cx)) {
                        Ok(()) => {}
                        Err(e) => return Poll::Ready(Err(e)),
                    }
                }
                Err(error) => return Poll::Ready(Err(error)),
            }
        }
    }

    /// Attempts to shutdown this writer.
    pub fn poll_shutdown(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<IoResult<()>> {
        loop {
            match self.stream.shutdown(Shutdown::Write) {
                Ok(()) => return Poll::Ready(Ok(())),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    match ready!(self.handle.rt().poll(self.token, Interest::WRITABLE, cx)) {
                        Ok(()) => {}
                        Err(e) => return Poll::Ready(Err(e)),
                    }
                }
                Err(error) => return Poll::Ready(Err(error)),
            }
        }
    }
}

impl Drop for TcpStream {
    fn drop(&mut self) {
        let _drop = self.handle.rt().deregister(self.token, &mut self.stream);
    }
}

/// A UDP socket.
#[derive(Debug)]
pub struct UdpSocket {
    /// Underlying Mio [`UdpSocket`].
    ///
    /// [`UdpSocket`]: mio::net::UdpSocket
    socket: MioUdpSocket,
    /// Mio event Token used for registration and polling.
    token: Token,
    /// The [`Handle`] to the current [`Runtime`].
    ///
    /// [`Runtime`]: crate::runtime::Runtime
    handle: Handle,
}

impl UdpSocket {
    /// This function will create a new UDP socket and attempt to bind it to the
    /// `addr` provided.
    pub fn bind(handle: Handle, addr: SocketAddr) -> IoResult<Self> {
        let mut socket = MioUdpSocket::bind(addr)?;

        let token = handle
            .rt()
            .register(&mut socket, Interest::READABLE | Interest::WRITABLE)?;

        Ok(UdpSocket {
            socket,
            token,
            handle,
        })
    }

    /// Receives a single datagram message on the socket.
    pub async fn recv_from(&self, buf: &mut [u8]) -> IoResult<(usize, SocketAddr)> {
        loop {
            match self.socket.recv_from(buf) {
                Ok(value) => return Ok(value),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    poll_fn(|cx| self.handle.rt().poll(self.token, Interest::READABLE, cx)).await?;
                }
                Err(error) => return Err(error),
            }
        }
    }

    /// Sends data on the socket to the given address.
    pub async fn send_to(&self, buf: &[u8], target: SocketAddr) -> IoResult<usize> {
        loop {
            match self.socket.send_to(buf, target) {
                Ok(value) => return Ok(value),
                Err(error) if error.kind() == ErrorKind::WouldBlock => {
                    poll_fn(|cx| self.handle.rt().poll(self.token, Interest::WRITABLE, cx)).await?;
                }
                Err(error) => return Err(error),
            }
        }
    }
}

impl Drop for UdpSocket {
    fn drop(&mut self) {
        let _drop = self.handle.rt().deregister(self.token, &mut self.socket);
    }
}
