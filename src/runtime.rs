// Copyright 2024 Ahmed Charles <me@ahmedcharles.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! The Stio runtime.

use crate::task::{Id, Task};

use std::{
    cell::RefCell,
    collections::VecDeque,
    fmt,
    future::Future,
    io::{Error as IoError, ErrorKind, Result as IoResult},
    mem::{replace, size_of},
    rc::{Rc, Weak},
    task::{Context, Poll, Waker},
};

use mio::{
    event::{Event, Source},
    Events, Interest, Registry, Token,
};
use slab::Slab;

/// The state of each reactor registration.
///
/// Three states are required due to the way future's poll method works.
///
/// When attempting to read from a source which is not ready, the following
/// sequence will occur:
///
/// 1. The task will attempt to read from the source and will receive
///    a would block error.
/// 2. The task will poll the reactor, specifying its read interest. The reactor
///    will change the state to [`Pending`] and return pending.
/// 3. The source will eventually become ready to be read and the reactor will
///    change the state to [`Ready`] and wake the waker.
/// 4. The task will poll the reactor again, due to future state machines only
///    progressing when poll returns ready. The reactor will change the state to
///    [`Neutral`] and return ready.
/// 5. The task will attempt to read from the source, which will probably
///    succeed, because spurious wake ups are possible.
///
/// [`Neutral`]: State::Neutral
/// [`Pending`]: State::Pending
/// [`Ready`]: State::Ready
#[derive(Debug)]
enum State {
    /// The source is in an unknown readiness state and the task has not tried
    /// to poll this particular source yet.
    Neutral,

    /// The source is not ready and the task is waiting for the source to become
    /// ready.
    ///
    /// - [`Interest`] - the events which should result in a call to [`wake`].
    /// - [`Waker`] - the waker associated with this task.
    ///
    /// [`Interest`]: mio::Interest
    /// [`Waker`]: core::task::Waker
    /// [`wake`]: State::wake()
    Pending(Interest, Waker),

    /// The source is ready, but the task has not been polled again yet.
    Ready,
}

static_assertions::const_assert_eq!(size_of::<State>(), 24);

impl State {
    /// Wake the appropriate task, if appropriate, based on interest.
    fn wake(&mut self, event: &Event) {
        match *self {
            State::Neutral => *self = State::Ready,
            State::Pending(interests, _) => {
                if interests.is_readable() == event.is_readable()
                    || interests.is_writable() == event.is_writable()
                {
                    if let State::Pending(_, waker) = replace(self, State::Ready) {
                        waker.wake();
                    }
                }
            }
            State::Ready => {}
        }
    }
}

/// Shared runtime state, between [`Runtime`], [`Handle`] and [`RUNTIME`].
#[derive(Debug)]
pub(crate) struct RuntimeInner {
    /// Allows polling for readiness events.
    poll: RefCell<mio::Poll>,
    /// Allows registering readiness events.
    registry: Registry,
    /// Tracks the state of events, once registered and polled.
    states: RefCell<Slab<State>>,
    /// Stores tasks which are being executed.
    tasks: RefCell<Slab<Task>>,
    /// Queue of runnable tasks.
    queue: RefCell<VecDeque<Id>>,
}

impl RuntimeInner {
    /// Create a new shared runtime state.
    fn new() -> IoResult<RuntimeInner> {
        let poll = mio::Poll::new()?;
        let registry = poll.registry().try_clone()?;

        Ok(RuntimeInner {
            poll: RefCell::new(poll),
            registry,
            states: RefCell::new(Slab::new()),
            tasks: RefCell::new(Slab::new()),
            queue: RefCell::new(VecDeque::with_capacity(16)),
        })
    }

    /// Register an event source with the Mio poll instance.
    pub(crate) fn register<S: Source + ?Sized>(
        &self,
        source: &mut S,
        interests: Interest,
    ) -> IoResult<Token> {
        let token;
        {
            let mut guard = self.states.borrow_mut();
            token = Token(guard.insert(State::Neutral));
        }
        self.registry.register(source, token, interests)?;
        Ok(token)
    }

    /// Deregister an event source with the Mio poll instance.
    pub(crate) fn deregister<S: Source + ?Sized>(
        &self,
        token: Token,
        source: &mut S,
    ) -> IoResult<()> {
        {
            let mut guard = self.states.borrow_mut();
            drop(guard.try_remove(token.0));
        }
        self.registry.deregister(source)
    }

    /// Used by sources to wait for readiness events.
    pub(crate) fn poll(
        &self,
        token: Token,
        interests: Interest,
        cx: &mut Context<'_>,
    ) -> Poll<IoResult<()>> {
        debug_assert!(
            interests.is_readable() || interests.is_writable(),
            "Either READABLE or WRITABLE interest is required."
        );
        let mut guard = self.states.borrow_mut();
        let state = &mut guard[token.0];
        match *state {
            State::Neutral => {
                *state = State::Pending(interests, cx.waker().clone());
                Poll::Pending
            }
            State::Pending(i, ref waker) => {
                if !waker.will_wake(cx.waker()) || i != interests {
                    *state = State::Pending(interests, cx.waker().clone());
                }
                Poll::Pending
            }
            State::Ready => {
                *state = State::Neutral;
                Poll::Ready(Ok(()))
            }
        }
    }

    /// Polls the Mio poll instance for readiness events.
    fn poll_mio_once(&self, events: &mut Events) {
        match self.poll.borrow_mut().poll(events, None) {
            Ok(()) => {}
            Err(e) if e.kind() == ErrorKind::Interrupted => {}
            Err(e) => panic!("unexpected error when polling mio: {e:?}"),
        }

        for event in events.iter() {
            let mut guard = self.states.borrow_mut();

            if let Some(state) = guard.get_mut(event.token().0) {
                state.wake(event);
            }
        }
    }

    /// Enqueue a task to be executed.
    fn enqueue(&self, task: Id) {
        self.queue.borrow_mut().push_back(task);
    }

    /// Clone a waker, via incrementing its task's reference count.
    fn clone_waker(&self, task: Id) -> Id {
        let count = &self.tasks.borrow()[task.0].count;
        count.set(count.get() + 1);
        task
    }

    /// Drop a waker, via decrementing its task's reference count.
    fn drop_waker(&self, task: Id) {
        let count = &self.tasks.borrow()[task.0].count;
        count.set(count.get() - 1);
    }
}

thread_local! {
    /// Thread local runtime.
    ///
    /// Only one runtime per thread may be created at a time.
    static RUNTIME: RefCell<Weak<RuntimeInner>> = const { RefCell::new(Weak::new()) };
}

/// The Stio runtime.
#[derive(Debug)]
pub struct Runtime(Rc<RuntimeInner>);

impl Runtime {
    /// Creates a new runtime instance.
    pub fn new() -> IoResult<Runtime> {
        let ri = Rc::new(RuntimeInner::new()?);
        RUNTIME.with_borrow_mut(|r| {
            if r.strong_count() != 0 {
                return Err(IoError::from(ErrorKind::AlreadyExists));
            }
            *r = Rc::downgrade(&ri);
            Ok(())
        })?;
        Ok(Runtime(ri))
    }

    /// Returns a handle to the runtime's spawner.
    pub fn handle(&self) -> Handle {
        Handle(Rc::downgrade(&self.0))
    }

    /// Runs a future to completion on the Stio runtime. This is the runtime's
    /// entry point.
    pub fn block_on<F: Future>(&self, future: F) -> F::Output {
        let mut future = std::pin::pin!(future);

        self.0
            .enqueue(Task::add(&self.0.tasks, None, self.handle()));

        let mut events = Events::with_capacity(1024);
        loop {
            let next = self.0.queue.borrow_mut().pop_front();
            let (ret, remove) = if let Some(task_id) = next {
                let tasks = self.0.tasks.borrow();
                let Some(task) = tasks.get(task_id.0) else {
                    continue;
                };
                let remove_id = task_id.0;
                let waker = waker::waker(task_id);
                let mut context = Context::from_waker(&waker);

                match task.future.as_ref() {
                    Some(future) => match future.borrow_mut().as_mut().poll(&mut context) {
                        Poll::Ready(()) => (None, Some(remove_id)),
                        Poll::Pending => (None, None),
                    },
                    None => match future.as_mut().poll(&mut context) {
                        Poll::Ready(r) => (Some(r), Some(remove_id)),
                        Poll::Pending => (None, None),
                    },
                }
            } else {
                self.0.poll_mio_once(&mut events);
                (None, None)
            };
            if let Some(id) = remove {
                drop(self.0.tasks.borrow_mut().remove(id));
            }
            if let Some(r) = ret {
                return r;
            }
        }
    }
}

/// Handle to the runtime.
///
/// The handle is internally reference-counted and can be freely cloned. A
/// handle can be obtained using the `Runtime::handle` method.
///
/// [`Runtime::handle`]: crate::runtime::Runtime::handle()
#[derive(Clone)]
pub struct Handle(Weak<RuntimeInner>);

impl Handle {
    /// Spawns a future onto the Stio runtime.
    ///
    /// # Panics
    ///
    /// This function panics if the [`Runtime`] is no longer running.
    pub fn spawn<F>(&self, future: F)
    where
        F: Future<Output = ()> + 'static,
    {
        let h = self.rt();

        h.enqueue(Task::add(&h.tasks, Some(Box::pin(future)), self.clone()));
    }

    /// Return a pointer to the current shared runtime state.
    pub(crate) fn rt(&self) -> Rc<RuntimeInner> {
        self.0.upgrade().expect("no Runtime for this handle")
    }
}

impl fmt::Debug for Handle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Handle").finish()
    }
}

/// Module for unsafe waker code.
#[allow(unsafe_code)]
mod waker {
    use crate::{runtime::RUNTIME, task::Id};
    use std::task::{RawWaker, RawWakerVTable, Waker};

    /// Called when the waker gets cloned.
    fn clone(ptr: *const ()) -> RawWaker {
        let ptr = RUNTIME.with_borrow(|r| {
            let r = r.upgrade().expect("no Runtime for this thread");
            r.clone_waker(Id(ptr as usize)).0
        }) as *const ();

        RawWaker::new(ptr, &WAKER_VTABLE)
    }

    /// Called when wake is called on the waker. The pointer is owned.
    fn wake(ptr: *const ()) {
        RUNTIME.with_borrow(|r| {
            let r = r.upgrade().expect("no Runtime for this thread");
            r.enqueue(Id(ptr as usize));
        });
    }

    /// Called when `wake_by_ref` is called on the waker. The pointer is a
    /// reference.
    fn wake_by_ref(ptr: *const ()) {
        RUNTIME.with_borrow(|r| {
            let r = r.upgrade().expect("no Runtime for this thread");
            // Cloning the waker is required because drop on this waker will
            // still be called.
            r.enqueue(r.clone_waker(Id(ptr as usize)));
        });
    }

    /// Called when the waker gets dropped.
    fn drop(ptr: *const ()) {
        RUNTIME.with_borrow(|r| {
            let r = r.upgrade().expect("no Runtime for this thread");
            r.drop_waker(Id(ptr as usize));
        });
    }

    /// A virtual function pointer table using the functions above.
    const WAKER_VTABLE: RawWakerVTable = RawWakerVTable::new(clone, wake, wake_by_ref, drop);

    /// Creates a waker for the relevant task with the vtable above.
    pub(super) fn waker(task_id: Id) -> Waker {
        // SAFETY: The functions above never dereference the data pointer, so
        //         constructing this from an arbitrary usize value is fine.
        unsafe { Waker::from_raw(RawWaker::new(task_id.0 as *const (), &WAKER_VTABLE)) }
    }
}
