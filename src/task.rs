// Copyright 2024 Ahmed Charles <me@ahmedcharles.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Asynchronous green-threads.

use crate::runtime::Handle;

use slab::Slab;
use std::{
    cell::{Cell, RefCell},
    fmt,
    future::Future,
    pin::Pin,
};

/// An ID that uniquely identifies a task relative to other currently running
/// tasks.
#[derive(Clone)]
pub(crate) struct Id(pub(crate) usize);

impl fmt::Debug for Id {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

/// The state associated with a task, including its future.
pub(crate) struct Task {
    /// The future assocated with a task, [`None`] refers to the future within
    /// the current [`block_on`].
    ///
    /// [`block_on`]: crate::runtime::Runtime::block_on()
    pub(crate) future: Option<RefCell<Pin<Box<dyn Future<Output = ()> + 'static>>>>,
    /// The [`Handle`] to the current [`Runtime`].
    ///
    /// [`Runtime`]: crate::runtime::Runtime
    handle: Handle,
    /// The ID that uniquely identifies this task.
    id: Id,
    /// The reference count of how many wakers are currently referring to this
    /// task.
    pub(crate) count: Cell<usize>,
}

impl Task {
    /// Create a new [`Task`] from the given `future` and add it to the list of `tasks`.
    pub(crate) fn add(
        tasks: &RefCell<Slab<Task>>,
        future: Option<Pin<Box<dyn Future<Output = ()>>>>,
        handle: Handle,
    ) -> Id {
        let future = future.map(RefCell::new);
        let mut tasks = tasks.borrow_mut();
        let vacant = tasks.vacant_entry();
        let id = vacant.key();
        _ = vacant.insert(Task {
            future,
            handle,
            id: Id(id),
            count: Cell::new(1),
        });
        Id(id)
    }
}

impl fmt::Debug for Task {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Task")
            .field("future", &self.future.as_ref().map(|_| "Future"))
            .field("handle", &self.handle)
            .field("id", &self.id)
            .field("count", &self.count.get())
            .finish()
    }
}
